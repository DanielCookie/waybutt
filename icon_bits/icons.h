static const uint16_t IconHeight = 14;
static const uint16_t IconWidth  = 17;

enum {
	IconPistol,   IconMount,
	IconRhomboid, IconStripes,
	IconSquares,  IconCatSymbol,
	IconAmount,
};

static const uint32_t PistolBits[] = {
_,O,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,_,_,_,_,O,O,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,O,O,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,O,O,_,_,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,_,_,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
O,O,O,O,O,_,_,_,_,_,_,_,_,_,_,_,_,
};

static const uint32_t MountBits[] = {
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,O,O,O,O,_,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
};

static const uint32_t RhomboidBits[] = {
_,_,_,_,_,_,O,O,O,O,_,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,_,O,O,O,O,O,O,O,O,O,O,_,_,_,_,
_,_,_,_,O,O,O,O,O,O,O,O,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,_,_,O,O,O,O,_,_,_,_,_,_,_,
};

static const uint32_t StripesBits[] = {
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,O,O,O,O,O,O,O,O,O,O,O,O,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
_,_,_,_,_,O,O,O,O,O,O,_,_,_,_,_,_,
};

static const uint32_t SquaresBits[] = {
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,O,O,O,O,O,O,O,_,_,_,_,_,_,_,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
_,_,_,_,_,_,_,_,O,O,O,O,O,O,O,_,_,
};

static const uint32_t CatSymbolBits[] = {
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,O,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
_,_,_,O,O,O,O,_,_,O,O,O,O,_,_,_,_,
};

static const const uint32_t *Icons[] = {
	PistolBits,   MountBits,
	RhomboidBits, StripesBits,
	SquaresBits,  CatSymbolBits,
};
