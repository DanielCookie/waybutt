#define FONT_NUMBER 4
static const char *FnNames[FONT_NUMBER] = {
	"monospace:pixelsize=13:style=Bold:antialias=true",
	"Koruri:pixelsize=13:Semibold:antialias=true",
	"Blobmoji:pixelsize=13:antialias=true:autohint=true",
	"Symbola:pixelsize=13:antialias=true:autohint=true",
};

static const uint32_t ColBg     = 0x0C0C0Ced;
static const uint32_t ColBorder = 0x161616ff;
static const uint32_t ColDesc   = 0x777794ff;
static const uint32_t ColNotice = 0xEB8BB2ff;
static const uint32_t ColHelp   = 0x5A5977ff;

static const uint32_t ColButtonBg = 0x0C0C0Ced;
static const uint32_t ColButtonFg = 0x9292B5ff;
static const uint32_t ColButtonUl = 0x161616ed;

static const uint32_t ColSelButtonBg = 0x0C0C0Ced;
static const uint32_t ColSelButtonFg = 0xA39CD9ff;
static const uint32_t ColSelButtonUl = 0x282A58ed;

static const uint32_t ColHilButtonBg = 0x0C0C0Ced;
static const uint32_t ColHilButtonFg = 0x81A1C1ff;
static const uint32_t ColHilButtonUl = 0x161616ed;

static const uint32_t ColSelHilButtonBg = 0x0C0C0Ced;
static const uint32_t ColSelHilButtonFg = 0xEBA28Bff;
static const uint32_t ColSelHilButtonUl = 0x8FBCBBed;

static const uint32_t ColImageFg      = 0xEBCB8Bed;
static const uint32_t ColImageShading = 0xEBA28Bed;
static const uint32_t ColImageBorder  = 0x404040ed;

static const uint16_t DimLineHeight = 22;
static const uint16_t DimTextLevel  = 16; // text level from top of line
static const uint8_t  BorderWidth   = 5;
static const uint8_t  ButtonUnderlineWidth = 4;

static const char *HelpText = "Enter: Select item, Ctrl-c: Cancel";
