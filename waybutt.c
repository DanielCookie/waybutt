#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <sys/mman.h>
#include <unistd.h>
#include <xkbcommon/xkbcommon.h>
#include <assert.h>
#include <sys/timerfd.h>
#include <fontconfig.h>
#include <pixman.h>
#include <fcntl.h>
#include <fcft/fcft.h>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "xdg-output-unstable-v1-client-protocol.h"

#include "utf8.h"
#include "pool_buffer.h"
#include "config.h"

#define O 0xFFFFFFff
#define _ 0x00000000

#include "icon_bits/icons.h"
#include "icon_bits/key.h"
#include "icon_bits/padlock.h"
#include "icon_bits/power.h"

#undef O
#undef _

static const const uint32_t *ImageLayers[] = {
	KeyFgBits, KeyShadingBits, KeyBorderBits,
	PadlockFgBits, PadlockShadingBits, PadlockBorderBits,
	PowerFgBits, PowerShadingBits, PowerBorderBits,
};

enum {
	ImageKey = 0,
	ImagePadlock = 3,
	ImagePower = 6,
	ImageNone = 9,
};

enum {
	RunFailure = -1,
	RunSuccess = 0,
	RunRunning = 1,
};

enum {
	ExitSuccess = 0,
	ExitFailure = 1,
	ExitTimedOut = 2,
};

enum {
	PointerButtonPressed = 1,
	PointerButtonLeftClick = 272,
};

struct desc_line {
	struct fcft_text_run *run;
	struct desc_line *next;
};

struct button {
	char *text;
	struct fcft_text_run *run;
	bool highlighted;
	struct button *next, *prev;
	uint16_t text_advance;
	uint16_t x, width;
};

struct output {
	struct waybutt_state *waybutt;
	struct wl_output *output;
	struct zxdg_output_v1 *xdg_output;
	int32_t scale;
};

struct button_canvas {
	pixman_color_t bg;
	pixman_image_t *fg;
	pixman_color_t ul;
};

struct pix_canvas {
	pixman_color_t bg;     pixman_color_t border;
	pixman_color_t notice; pixman_color_t desc;
	pixman_color_t help;

	struct button_canvas butt;  struct button_canvas sbutt;
	struct button_canvas hbutt; struct button_canvas shbutt;
};

struct hex_colors {
	uint32_t bg;   uint32_t desc; uint32_t notice;
	uint32_t help; uint32_t border;
	uint32_t butt[3];  uint32_t sbutt[3];
	uint32_t hbutt[3]; uint32_t shbutt[3];
	uint32_t image_layers[3];
};

struct dimensions {
	uint8_t  line_height;   uint8_t  text_level;
	uint8_t  padding;       uint8_t  border_width;
	uint16_t width;         uint16_t height;
	uint16_t help_width;
	uint16_t upper_padding; uint16_t button_ul_width;
	uint16_t buttons_width; uint16_t buttons_height;
};

struct waybutt_state {
	struct output *output;
	char *output_name;

	struct wl_display *display;
	struct wl_compositor *compositor;
	struct wl_shm *shm;
	struct wl_seat *seat;
	struct wl_keyboard *keyboard;

	struct wl_pointer *pointer;
	wl_fixed_t ptr_surface_x, ptr_surface_y;

	struct zwlr_layer_shell_v1 *layer_shell;
	struct zxdg_output_manager_v1 *output_manager;

	struct wl_surface *surface;
	struct zwlr_layer_surface_v1 *layer_surface;

	struct xkb_context *xkb_context;
	struct xkb_keymap *xkb_keymap;
	struct xkb_state *xkb_state;

	struct pool_buffer buffers[2];
	struct pool_buffer *current;

	struct hex_colors hex;
	struct dimensions dim;
	struct pix_canvas canvas;

	const char **fn_names[FONT_NUMBER];
	const char *help_str;
	const char *notice_str;
	struct fcft_text_run *help_run;
	struct fcft_font *font;

	int32_t repeat_timer;
	int32_t repeat_delay;
	int32_t repeat_period;
	uint32_t repeat_key;
	enum wl_keyboard_key_state repeat_key_state;
	xkb_keysym_t repeat_sym;

	uint8_t image_width;
	uint8_t image_height;
	uint8_t image_type;

	bool bottom;
	int8_t run;

	struct desc_line *description;
	uint8_t desc_lines;
	uint16_t desc_width;

	struct button *buttons, *selection, *prev_selection, *last;
	uint16_t buttons_x, buttons_y;
	uint8_t initial_sel_idx;
};

// Principal functions
static void waybutt_init(struct waybutt_state *ws, struct dimensions *dim);
static void font_init(struct waybutt_state *ws);
static void read_stdin(struct waybutt_state *ws);
static void waybutt_create_surface(struct waybutt_state *ws);
static void free_description(struct waybutt_state *ws);
static void free_buttons(struct waybutt_state *ws);
static void waybutt_fini(struct waybutt_state *ws);
static inline void surface_fini(struct waybutt_state *ws);
static inline void canvas_fini(struct pix_canvas *canvas);
static inline void font_fini(struct waybutt_state *ws);
static inline void compositor_fini(struct waybutt_state *ws);
static struct pix_canvas canvas_init(struct hex_colors *hex);
static void draw_frame(struct waybutt_state *ws);
static void draw_first_frame(struct waybutt_state *ws);
static void keypress(struct waybutt_state *ws,
	enum wl_keyboard_key_state key_state, xkb_keysym_t sym);
static void pointer_axis(void *data, struct wl_pointer *pointer,
	uint32_t time, uint32_t axis, wl_fixed_t value);
static void pointer_motion(void *data, struct wl_pointer *pointer,
	uint32_t time, wl_fixed_t surface_x, wl_fixed_t surface_y);
static void pointer_button(void *data, struct wl_pointer *pointer,
	uint32_t serial, uint32_t time, uint32_t button, uint32_t state);

static void noop() {}

static void eprintf(const char *fmt, ...) {
	va_list ap;

	fprintf(stderr, "%s: ", PROGNAME);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

static inline void fill_box_solid(pixman_image_t *pixman,
pixman_color_t *color, uint16_t x1, uint16_t x2, uint16_t y1, uint16_t y2) {

	pixman_image_fill_boxes(PIXMAN_OP_SRC, pixman, color, 1,
		&(pixman_box32_t) {.x1 = x1, .x2 = x2, .y1 = y1, .y2 = y2});
}

static inline void draw_glyph(pixman_image_t *pixman,
const struct fcft_glyph *rune, pixman_image_t *fill, uint16_t x, uint16_t y) {

	if (pixman_image_get_format(rune->pix) == PIXMAN_a8r8g8b8) {
		pixman_image_composite32(PIXMAN_OP_OVER, rune->pix, NULL, pixman,
			0, 0, 0, 0, x + rune->x, y - rune->y, rune->width, rune->height);
	}
	else {
		pixman_image_composite32(PIXMAN_OP_OVER, fill, rune->pix, pixman,
			0, 0, 0, 0, x + rune->x, y - rune->y, rune->width, rune->height);
	}
}

static inline const pixman_color_t hex2pixman(uint32_t hex_color) {
	return (pixman_color_t) {
		.red   = (hex_color >> 0x18 & 0xFF) * 0xFF,
		.green = (hex_color >> 0x10 & 0xFF) * 0xFF,
		.blue  = (hex_color >> 0x8  & 0xFF) * 0xFF,
		.alpha = (hex_color         & 0xFF) * 0xFF,
	};
}

static char *strip_spaces_lr(char *start, char *end, bool *highlighted) {
	char *p;

	for (p = end;   *p == ' ' || *p == '\t'; p--) *p = '\0'; // strip right
	for (p = start; *p == ' ' || *p == '\t'; p++) *p = '\0'; // strip left

	if (*p == '!') {
		*p = '\0';
		for (p = ++p; *p == ' ' || *p == '\t'; p++) *p = '\0'; // strip left
		*highlighted = true;
	}
	else *highlighted = false;

	return p;
}

static void parse_hex_color(char *str, uint32_t *color) {
	if (str[0] == '#') str++;

	size_t len = strnlen(str, BUFSIZ);

	if (len != 6 && len != 8)
		eprintf("Color format must be '[#]rrggbb[aa]'\n");

	char *ptr;
	uint32_t _val = strtoul(str, &ptr, 16);
	if (*ptr != '\0') eprintf("Could not convert string to hex number");

	*color = len == 6 ? ((_val << 8) | 0xff) : _val;
}

static void parse_color_triad(char *str, uint32_t *colors) {
	char *p, *comma;
	size_t i;

	for (p = str, i = 0; i < 3; p = comma + 1, i++) {
		comma = strchr(p, ',');
		if (comma != NULL) *comma = '\0';

		parse_hex_color(p, colors + i);
	}
}

static void set_line_height(struct dimensions *dim, char *height_str) {
	uint8_t i;
	for (i = 0; i <= 3; i++) if (height_str[i] == ':') break;

	if (2 < i) dim->line_height = atoi(height_str);
	else {
		height_str[i] = '\0';
		dim->line_height = atoi(height_str);
		dim->text_level = dim->line_height - atoi(height_str + ++i);
	}
}

static void surface_enter(void *data,
struct wl_surface *surface, struct wl_output *wl_output) {

	struct waybutt_state *ws = data;
	ws->output = wl_output_get_user_data(wl_output);
	wl_surface_set_buffer_scale(ws->surface, ws->output->scale);
	wl_surface_commit(ws->surface);
	draw_frame(ws);
}

static const struct wl_surface_listener surface_listener = {
	.enter = surface_enter,
	.leave = noop,
};

static void layer_surface_configure(void *data,
struct zwlr_layer_surface_v1 *surface,
uint32_t serial, uint32_t width, uint32_t height) {

	struct waybutt_state *ws = data;
	ws->dim.width = width;
	ws->dim.height = height;
	zwlr_layer_surface_v1_ack_configure(surface, serial);
}

static void layer_surface_closed(void *data,
struct zwlr_layer_surface_v1 *surface) {

	struct waybutt_state *ws = data;
	ws->run = RunSuccess;
}

struct zwlr_layer_surface_v1_listener layer_surface_listener = {
	.configure = layer_surface_configure,
	.closed = layer_surface_closed,
};

static void output_scale(void *data,
struct wl_output *wl_output, int32_t factor) {

	struct output *output = data;
	output->scale = factor;
}

static void output_name(void *data, struct zxdg_output_v1 *xdg_output,
const char *name) {

	struct output *output = data;
	struct waybutt_state *ws = output->waybutt;
	char *outname = ws->output_name;

	if (!ws->output && outname && strcmp(outname, name) == 0) ws->output = output;
}

struct wl_output_listener output_listener = {
	.geometry = noop,
	.mode = noop,
	.done = noop,
	.scale = output_scale,
};

struct zxdg_output_v1_listener xdg_output_listener = {
	.logical_position = noop,
	.logical_size = noop,
	.done = noop,
	.name = output_name,
	.description = noop,
};

static void keyboard_keymap(void *data, struct wl_keyboard *wl_keyboard,
uint32_t format, int32_t fd, uint32_t size) {

	struct waybutt_state *ws = data;

	if (format != WL_KEYBOARD_KEYMAP_FORMAT_XKB_V1) {
		close(fd);
		ws->run = RunFailure;
		return;
	}

	char *map_shm = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
	if (map_shm == MAP_FAILED) {
		close(fd);
		ws->run = RunFailure;
		return;
	}

	ws->xkb_keymap = xkb_keymap_new_from_string(ws->xkb_context,
		map_shm, XKB_KEYMAP_FORMAT_TEXT_V1, 0);
	munmap(map_shm, size);
	close(fd);
	ws->xkb_state = xkb_state_new(ws->xkb_keymap);
}

static void keyboard_repeat(struct waybutt_state *ws) {
	keypress(ws, ws->repeat_key_state, ws->repeat_sym);
	struct itimerspec spec = { 0 };

	spec.it_value.tv_sec = ws->repeat_period / 1000;
	spec.it_value.tv_nsec = (ws->repeat_period % 1000) * 1000000l;
	timerfd_settime(ws->repeat_timer, 0, &spec, NULL);
}

static void keyboard_key(void *data, struct wl_keyboard *wl_keyboard,
uint32_t serial, uint32_t time, uint32_t key, uint32_t _key_state) {

	struct waybutt_state *ws = data;
	enum wl_keyboard_key_state key_state = _key_state;
	xkb_keysym_t sym = xkb_state_key_get_one_sym(ws->xkb_state, key + 8);

	keypress(ws, key_state, sym);

	if (key_state == WL_KEYBOARD_KEY_STATE_PRESSED && 0 <= ws->repeat_period) {
		ws->repeat_key_state = key_state;
		ws->repeat_sym = sym;
		ws->repeat_key = key;

		struct itimerspec spec = { 0 };
		spec.it_value.tv_sec = ws->repeat_delay / 1000;
		spec.it_value.tv_nsec = (ws->repeat_delay % 1000) * 1000000l;
		timerfd_settime(ws->repeat_timer, 0, &spec, NULL);
	}
	else if (key_state == WL_KEYBOARD_KEY_STATE_RELEASED && key == ws->repeat_key) {
		struct itimerspec spec = { 0 };
		timerfd_settime(ws->repeat_timer, 0, &spec, NULL);
	}
}

static void keyboard_repeat_info(void *data,
struct wl_keyboard *wl_keyboard, int32_t rate, int32_t delay) {

	struct waybutt_state *ws = data;
	ws->repeat_delay = delay;

	if (0 < rate) ws->repeat_period = 1000 / rate;
	else ws->repeat_period = -1;
}

static void keyboard_modifiers(void *data, struct wl_keyboard *keyboard,
uint32_t serial, uint32_t mods_depressed, uint32_t mods_latched,
uint32_t mods_locked, uint32_t group) {

	struct waybutt_state *ws = data;
	xkb_state_update_mask(ws->xkb_state, mods_depressed, mods_latched,
		mods_locked, 0, 0, group);
}

static const struct wl_keyboard_listener keyboard_listener = {
	.keymap = keyboard_keymap,
	.enter = noop,
	.leave = noop,
	.key = keyboard_key,
	.modifiers = keyboard_modifiers,
	.repeat_info = keyboard_repeat_info,
};

static const struct wl_pointer_listener pointer_listener = {
	.enter = noop,
	.leave = noop,
	.motion = pointer_motion,
	.button = pointer_button,
	.axis = pointer_axis,
};

static void seat_capabilities(void *data, struct wl_seat *seat,
enum wl_seat_capability caps) {

	struct waybutt_state *ws = data;

	if (caps & WL_SEAT_CAPABILITY_KEYBOARD) {
		ws->keyboard = wl_seat_get_keyboard(seat);
		wl_keyboard_add_listener(ws->keyboard, &keyboard_listener, ws);
	}

	if (caps & WL_SEAT_CAPABILITY_POINTER) {
		if (ws->pointer == NULL) {
			ws->pointer = wl_seat_get_pointer(seat);
			wl_pointer_add_listener(ws->pointer, &pointer_listener, ws);
		}
	}
	else if (ws->pointer != NULL) {
		wl_pointer_release(ws->pointer);
		ws->pointer = NULL;
	}
}

const struct wl_seat_listener seat_listener = {
	.capabilities = seat_capabilities,
	.name = noop,
};

static void handle_global(void *data, struct wl_registry *registry,
uint32_t name, const char *interface, uint32_t version) {

	struct waybutt_state *ws = data;

	if (strcmp(interface, wl_compositor_interface.name) == 0) {
		ws->compositor = wl_registry_bind(registry, name,
			&wl_compositor_interface, 4);
	}
	else if (strcmp(interface, wl_shm_interface.name) == 0) {
		ws->shm = wl_registry_bind(registry, name, &wl_shm_interface, 1);
	}
	else if (strcmp(interface, wl_seat_interface.name) == 0) {
		struct wl_seat *seat = wl_registry_bind(registry, name,
			&wl_seat_interface, 4);
		wl_seat_add_listener(seat, &seat_listener, ws);
	}
	else if (strcmp(interface, zwlr_layer_shell_v1_interface.name) == 0) {
		ws->layer_shell = wl_registry_bind(registry, name,
			&zwlr_layer_shell_v1_interface, 1);
	}
	else if (strcmp(interface, zxdg_output_manager_v1_interface.name) == 0) {
		ws->output_manager = wl_registry_bind(registry, name,
			&zxdg_output_manager_v1_interface, 3);
	}
	else if (strcmp(interface, wl_output_interface.name) == 0) {
		struct output *output = calloc(1, sizeof(struct output));

		output->output = wl_registry_bind(registry, name, &wl_output_interface, 3);
		output->waybutt = ws;
		output->scale = 1;

		wl_output_set_user_data(output->output, output);
		wl_output_add_listener(output->output, &output_listener, output);

		if (ws->output_manager != NULL) {
			output->xdg_output = zxdg_output_manager_v1_get_xdg_output(
				ws->output_manager, output->output);
			zxdg_output_v1_add_listener(output->xdg_output,
				&xdg_output_listener, output);
		}
	}
}

static const struct wl_registry_listener registry_listener = {
	.global = handle_global,
	.global_remove = noop,
};

void draw_first_frame(struct waybutt_state *ws) {
	ws->current = get_next_buffer(ws->shm, ws->buffers,
		ws->dim.width, ws->dim.height, ws->output ? ws->output->scale : 1);

	if (!ws->current) return;

	pixman_image_t *pix_buf = ws->current->pixman, *fill, *key;
	pixman_color_t pix_col;
	struct button_canvas *bc;
	struct fcft_font *font = ws->font;
	struct pix_canvas *c = &ws->canvas;
	struct dimensions *d = &ws->dim;
	uint16_t x, y = 0, left_anchor;
	size_t i;
	const struct fcft_glyph *rune_p;
	struct fcft_text_run *run;
	uint32_t codepoint, state = UTF8_ACCEPT;
	struct desc_line *desc_line;
	struct button *butt;

	left_anchor = d->width / 4;

	fill_box_solid(pix_buf, &c->bg, 0, d->width,
		ws->bottom * d->border_width,
		d->height - !ws->bottom * d->border_width);
	fill_box_solid(pix_buf, &c->border, 0, d->width,
		!ws->bottom * (d->height - d->border_width),
		ws->bottom * d->border_width + !ws->bottom * d->height);

	y += ws->bottom * d->border_width + d->padding + d->upper_padding;

	// Description
	for (fill = pixman_image_create_solid_fill(&c->desc),
	     desc_line = ws->description; desc_line;
	     desc_line = desc_line->next, y += d->line_height) {

		run = desc_line->run;
		x = left_anchor + d->padding;

		for (i = 0, rune_p = *run->glyphs; i < run->count;
			x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

			draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
		}
	}
	pixman_image_unref(fill);

	// Notice
	if (ws->notice_str) {
		x = left_anchor + d->padding;
		fill = pixman_image_create_solid_fill(&c->notice);

		for (char *p = (char *)ws->notice_str; *p; p++) {
			if (utf8_decode(&state, &codepoint, *p)) continue;

			rune_p = fcft_rasterize_char_utf32(font, codepoint, FCFT_SUBPIXEL_NONE);
			draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
			x += rune_p->advance.x;
		}

		pixman_image_unref(fill);
		y += d->line_height;
	}

	// Buttons
	ws->buttons_y = y = y + d->padding;
	ws->buttons_x = x = left_anchor + d->padding;

	for (butt = ws->buttons; butt; x += 2 * d->padding, butt = butt->next) {
		run = butt->run;
		bc = butt->highlighted ? &c->hbutt : &c->butt;

		butt->x = x;
		butt->width = butt->text_advance + 2 * d->padding;

		fill_box_solid(pix_buf, &bc->bg, x, x + butt->width,
			y, y + d->line_height);

		fill_box_solid(pix_buf, &bc->ul, x, x + butt->width,
			y + d->line_height, y + d->buttons_height);

		for (x += d->padding, i = 0, rune_p = *run->glyphs; i < run->count;
		     x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

			draw_glyph(pix_buf, rune_p, bc->fg, x, y + d->text_level);
		}
	}

	d->buttons_width = x - d->padding - ws->buttons_x;

	// Help
	y += d->padding + d->buttons_height;
	run = ws->help_run;

	x = left_anchor + d->padding;
	for (fill = pixman_image_create_solid_fill(&c->help),
	     i = 0, rune_p = *run->glyphs; i < run->count;
	     x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

		draw_glyph(pix_buf, rune_p, fill, x, y + d->text_level);
	}
	pixman_image_unref(fill);

	// Image
	y = ws->bottom * d->border_width +
	    (d->height - d->border_width) / 2 - ws->image_height / 2;
	x = left_anchor - 2 * d->padding - ws->image_width;

	for (i = 0; i < 3; i++) {
		pix_col = hex2pixman(ws->hex.image_layers[i]);
		fill = pixman_image_create_solid_fill(&pix_col);

		key = pixman_image_create_bits(PIXMAN_a8r8g8b8, ws->image_width,
		      ws->image_height,
		      (uint32_t *)ImageLayers[ws->image_type + i], ws->image_width * 4);

		pixman_image_composite32(PIXMAN_OP_OVER, fill, key, pix_buf,
			0, 0, 0, 0, x, y, ws->image_width, ws->image_height);

		pixman_image_unref(key);
		pixman_image_unref(fill);
	}

	wl_surface_attach(ws->surface, ws->current->buffer, 0, 0);
	wl_surface_damage_buffer(ws->surface, 0, 0, d->width, d->height);
	wl_surface_commit(ws->surface);
}

void draw_frame(struct waybutt_state *ws) {
	ws->current = get_next_buffer(ws->shm, ws->buffers,
		ws->dim.width, ws->dim.height, ws->output ? ws->output->scale : 1);

	if (!ws->current) return;

	pixman_image_t *pix_buf = ws->current->pixman;

	struct pix_canvas *c = &ws->canvas;
	struct dimensions *d = &ws->dim;
	struct button *butt;
	struct button_canvas *bc;
	struct fcft_text_run *run;
	const struct fcft_glyph *rune_p;
	uint16_t x = ws->buttons_x, y = ws->buttons_y;
	size_t i;

	// Buttons
	for (butt = ws->buttons; butt; x += 2 * d->padding, butt = butt->next) {
		if (butt == ws->selection) {
			run = butt->run;
			bc = butt->highlighted ? &c->shbutt : &c->sbutt;

			wl_surface_damage_buffer(ws->surface, x, y,
				butt->width, d->buttons_height);

			fill_box_solid(pix_buf, &bc->bg, x, x + butt->width,
				y, y + d->line_height);

			fill_box_solid(pix_buf, &bc->ul, x, x + butt->width,
				y + d->line_height, y + d->buttons_height);

			for (x += d->padding, i = 0, rune_p = *run->glyphs; i < run->count;
			     x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

				draw_glyph(pix_buf, rune_p, bc->fg, x, y + d->text_level);
			}
		}
		else if (butt == ws->prev_selection) {
			run = butt->run;
			bc = butt->highlighted ? &c->hbutt : &c->butt;

			wl_surface_damage_buffer(ws->surface, x, y,
				butt->width, d->buttons_height);

			fill_box_solid(pix_buf, &bc->bg, x, x + butt->width,
				y, y + d->line_height);

			fill_box_solid(pix_buf, &bc->ul, x, x + butt->width,
				y + d->line_height, y + d->buttons_height);

			for (x += d->padding, i = 0, rune_p = *run->glyphs; i < run->count;
			     x += rune_p->advance.x, rune_p = run->glyphs[++i]) {

				draw_glyph(pix_buf, rune_p, bc->fg, x, y + d->text_level);
			}
		}
		else x += d->padding + butt->text_advance;
	}

	wl_surface_attach(ws->surface, ws->current->buffer, 0, 0);
	wl_surface_commit(ws->surface);
}

void keypress(struct waybutt_state *ws,
enum wl_keyboard_key_state key_state, xkb_keysym_t sym) {

	if (key_state != WL_KEYBOARD_KEY_STATE_PRESSED) return;

	bool ctrl = xkb_state_mod_name_is_active(ws->xkb_state,
		XKB_MOD_NAME_CTRL, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);
	bool shift = xkb_state_mod_name_is_active(ws->xkb_state,
		XKB_MOD_NAME_SHIFT, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);
	bool alt = xkb_state_mod_name_is_active(ws->xkb_state,
		XKB_MOD_NAME_ALT, XKB_STATE_MODS_DEPRESSED | XKB_STATE_MODS_LATCHED);

	char buf[8];

	switch (sym) {
	case XKB_KEY_KP_Enter:
	case XKB_KEY_Return:
		puts(ws->selection->text);
		fflush(stdout);
		ws->run = RunSuccess;
		break;

	case XKB_KEY_h:
	case XKB_KEY_k:
	case XKB_KEY_Left:
		if (ws->selection->prev) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->selection->prev;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_Tab:
		if (ctrl) {
			if (ws->selection->prev) {
				ws->prev_selection = ws->selection;
				ws->selection = ws->selection->prev;
				draw_frame(ws);
			}
		}
		else {
			if (ws->selection->next) {
				ws->prev_selection = ws->selection;
				ws->selection = ws->selection->next;
				draw_frame(ws);
			}
		}
		break;

	case XKB_KEY_l:
	case XKB_KEY_Right:
		if (ws->selection->next) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->selection->next;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_j:
		if (ctrl) {
			puts(ws->selection->text);
			fflush(stdout);
			ws->run = RunSuccess;
		}
		else if (ws->selection->next) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->selection->next;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_dollar:
		if (ws->selection != ws->last) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->last;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_0:
	case XKB_KEY_asciicircum:
		if (ws->selection != ws->buttons) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->buttons;
			draw_frame(ws);
		}
		break;

	case XKB_KEY_bracketleft:
	case XKB_KEY_c:
		if (ctrl) ws->run = RunFailure;
		break;

	case XKB_KEY_Escape:
		ws->run = RunFailure;
		break;

	default:
		break;
	}
}

void pointer_axis(void *data, struct wl_pointer *pointer,
uint32_t time, uint32_t axis, wl_fixed_t value) {

	struct waybutt_state *ws = data;

	if (0.0 < wl_fixed_to_double(value)) {
		if (ws->selection->next != NULL) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->selection->next;
			draw_frame(ws);
		}
	}
	else {
		if (ws->selection->prev != NULL) {
			ws->prev_selection = ws->selection;
			ws->selection = ws->selection->prev;
			draw_frame(ws);
		}
	}
}

void pointer_motion(void *data, struct wl_pointer *pointer,
uint32_t time, wl_fixed_t surface_x, wl_fixed_t surface_y) {

	struct waybutt_state *ws = data;
	struct dimensions *d = &ws->dim;

	uint16_t x = (uint16_t)(wl_fixed_to_double(surface_x)),
	         y = (uint16_t)(wl_fixed_to_double(surface_y));

	if (ws->buttons_y <= y && y <= ws->buttons_y + d->buttons_height) {
		struct button *butt;
		for (butt = ws->buttons; butt; butt = butt->next) {
			if (butt != ws->selection) {
				if (butt->x <= x && x <= butt->x + butt->width) {
					if (butt->x <= x && x <= butt->x + butt->width) {
						ws->prev_selection = ws->selection;
						ws->selection = butt;
						draw_frame(ws);
						break;
					}
				}
			}
		}
	}

	ws->ptr_surface_x = surface_x;
	ws->ptr_surface_y = surface_y;
}

void pointer_button(void *data, struct wl_pointer *pointer,
uint32_t serial, uint32_t time, uint32_t button, uint32_t state) {

	struct waybutt_state *ws = data;
	struct dimensions *d = &ws->dim;

	if (button == PointerButtonLeftClick) {
		if (state == PointerButtonPressed) {
			uint16_t x = (uint16_t)(wl_fixed_to_double(ws->ptr_surface_x)),
			         y = (uint16_t)(wl_fixed_to_double(ws->ptr_surface_y));

			if (ws->buttons_y <= y && y <= ws->buttons_y + d->buttons_height) {
				if (ws->selection->x <= x &&
				x <= ws->selection->x + ws->selection->width) {

					puts(ws->selection->text);
					fflush(stdout);
					ws->run = RunSuccess;
				}
			}
		}
	}
}

void waybutt_create_surface(struct waybutt_state *ws) {
	ws->surface = wl_compositor_create_surface(ws->compositor);
	wl_surface_add_listener(ws->surface, &surface_listener, ws);
	ws->layer_surface = zwlr_layer_shell_v1_get_layer_surface(
		ws->layer_shell, ws->surface, NULL, ZWLR_LAYER_SHELL_V1_LAYER_TOP, PROGNAME);
	assert(ws->layer_surface != NULL);

	uint32_t anchor =
		ZWLR_LAYER_SURFACE_V1_ANCHOR_LEFT | ZWLR_LAYER_SURFACE_V1_ANCHOR_RIGHT;

	anchor |= ws->bottom ?
		ZWLR_LAYER_SURFACE_V1_ANCHOR_BOTTOM : ZWLR_LAYER_SURFACE_V1_ANCHOR_TOP; 

	zwlr_layer_surface_v1_set_anchor(ws->layer_surface, anchor);
	zwlr_layer_surface_v1_set_size(ws->layer_surface, 0, ws->dim.height);
	zwlr_layer_surface_v1_set_exclusive_zone(ws->layer_surface, -1);
	zwlr_layer_surface_v1_set_keyboard_interactivity(ws->layer_surface, true);
	zwlr_layer_surface_v1_add_listener(ws->layer_surface,
		&layer_surface_listener, ws);

	wl_surface_commit(ws->surface);
	wl_display_roundtrip(ws->display);
}

inline void canvas_fini(struct pix_canvas *canvas) {
	pixman_image_unref(canvas->butt.fg);
	pixman_image_unref(canvas->hbutt.fg);
	pixman_image_unref(canvas->sbutt.fg);
	pixman_image_unref(canvas->shbutt.fg);
}

struct pix_canvas canvas_init(struct hex_colors *hex) {
	const pixman_color_t fill_butt_fg   = hex2pixman(hex->butt[1]);
	const pixman_color_t fill_sbutt_fg  = hex2pixman(hex->sbutt[1]);
	const pixman_color_t fill_hbutt_fg  = hex2pixman(hex->hbutt[1]);
	const pixman_color_t fill_shbutt_fg = hex2pixman(hex->shbutt[1]);

	return (struct pix_canvas) {
		.bg     = hex2pixman(hex->bg),
		.border = hex2pixman(hex->border),
		.notice = hex2pixman(hex->notice),
		.desc   = hex2pixman(hex->desc),
		.help   = hex2pixman(hex->help),

		.butt = {
			.bg = hex2pixman(hex->butt[0]),
			.fg = pixman_image_create_solid_fill(&fill_butt_fg),
			.ul = hex2pixman(hex->butt[2])},
		.sbutt = {
			.bg = hex2pixman(hex->sbutt[0]),
			.fg = pixman_image_create_solid_fill(&fill_sbutt_fg),
			.ul = hex2pixman(hex->sbutt[2])},
		.hbutt = {
			.bg = hex2pixman(hex->hbutt[0]),
			.fg = pixman_image_create_solid_fill(&fill_hbutt_fg),
			.ul = hex2pixman(hex->hbutt[2])},
		.shbutt = {
			.bg = hex2pixman(hex->shbutt[0]),
			.fg = pixman_image_create_solid_fill(&fill_shbutt_fg),
			.ul = hex2pixman(hex->shbutt[2])},
	};
}

void waybutt_init(struct waybutt_state *ws, struct dimensions *dim) {
	dim->buttons_height = dim->line_height + dim->button_ul_width;
	dim->padding = dim->line_height / 2;

	uint32_t height_from_key = ws->image_height + 2 * dim->padding,
	         height = (2 + ws->desc_lines) * dim->line_height
	                + 4 * dim->padding + dim->button_ul_width;

	if (height < height_from_key) {
		dim->height = height_from_key + dim->border_width;
		dim->upper_padding = height_from_key - height;
	} else {
		dim->height = height + dim->border_width;
		dim->upper_padding = 0;
	}

	dim->height = height < height_from_key ? height_from_key : height;
	dim->height += dim->border_width + (ws->notice_str ? dim->line_height : 0);

	size_t i;
	for (i = 1, ws->selection = ws->buttons; i < ws->initial_sel_idx;
	     i++, ws->selection = ws->selection->next) {

		if (ws->selection->next == NULL) {
			free_description(ws);
			free_buttons(ws);
			eprintf("Index %d exceeds button count\n", ws->initial_sel_idx);
		}
	}

	ws->prev_selection = ws->selection;
	ws->canvas = canvas_init(&ws->hex);

	ws->display = wl_display_connect(NULL);
	if (!ws->display) {
		canvas_fini(&ws->canvas);
		free_description(ws);
		free_buttons(ws);
		eprintf("Could not connect to wayland display\n");
	}

	ws->xkb_context = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
	if (!ws->xkb_context) {
		canvas_fini(&ws->canvas);
		free_description(ws);
		free_buttons(ws);
		wl_display_disconnect(ws->display);
		eprintf("Could not create xkb context\n");
	}

	ws->repeat_timer = timerfd_create(CLOCK_MONOTONIC, 0);
	assert(0 <= ws->repeat_timer);

	struct wl_registry *registry = wl_display_get_registry(ws->display);
	wl_registry_add_listener(registry, &registry_listener, ws);
	wl_display_roundtrip(ws->display);

	assert(ws->compositor != NULL);
	assert(ws->layer_shell != NULL);
	assert(ws->shm != NULL);
	assert(ws->output_manager != NULL);

	// Second roundtrip for xdg-output
	wl_display_roundtrip(ws->display);

	if (ws->output_name && !ws->output) {
		canvas_fini(&ws->canvas);
		free_description(ws);
		free_buttons(ws);
		compositor_fini(ws);
		eprintf("Output %s not found\n", ws->output_name);
	}
}

void font_init(struct waybutt_state *ws) {
	uint16_t advance;
	size_t i, rune_num;
	char *str_p;
	uint32_t state = UTF8_ACCEPT, codepoint;

	fcft_init(FCFT_LOG_COLORIZE_AUTO, 8, FCFT_LOG_CLASS_ERROR);
	fcft_set_scaling_filter(FCFT_SCALING_FILTER_LANCZOS3);

	ws->font = fcft_from_name(FONT_NUMBER, (const char **)(*ws->fn_names), NULL);
	if (!ws->font) eprintf("Could not load font[s]!\n");

	uint32_t help32[strlen(ws->help_str) - 1];

	// Make help run
	for (rune_num = 0, str_p = (char *)ws->help_str; *str_p; str_p++) {

		if (utf8_decode(&state, &codepoint, *str_p)) continue;
		help32[rune_num] = codepoint;
		rune_num++;
	}

	ws->help_run = fcft_rasterize_text_run_utf32(ws->font,
		rune_num, (const uint32_t *)help32, FCFT_SUBPIXEL_NONE);

	for (advance = 0, i = 0; i < rune_num; i++) {
		advance += ws->help_run->glyphs[i]->advance.x;
	}
	ws->dim.help_width = advance + 2 * ws->dim.padding;
}

inline void surface_fini(struct waybutt_state *ws) {
	zwlr_layer_surface_v1_destroy(ws->layer_surface);
	wl_surface_destroy(ws->surface);
	zwlr_layer_shell_v1_destroy(ws->layer_shell);
}

inline void font_fini(struct waybutt_state *ws) {
	fcft_text_run_destroy(ws->help_run);
	fcft_destroy(ws->font);
	fcft_fini();
}

inline void compositor_fini(struct waybutt_state *ws) {
	wl_compositor_destroy(ws->compositor);
	wl_shm_destroy(ws->shm);
	wl_pointer_destroy(ws->pointer);
	wl_keyboard_destroy(ws->keyboard);
	zxdg_output_manager_v1_destroy(ws->output_manager);
	wl_display_disconnect(ws->display);
}

void free_description(struct waybutt_state *ws) {
	struct desc_line *line;
	for (line = ws->description; line; line = line->next) {
		fcft_text_run_destroy(line->run);
		//free(line); // segfaults, why?
	}
}

void free_buttons(struct waybutt_state *ws) {
	struct button *butt;
	for (butt = ws->buttons; butt; butt = butt->next) {
		fcft_text_run_destroy(butt->run);
		free(butt->text);
		free(butt);
	}
}

void waybutt_fini(struct waybutt_state *ws) {
	canvas_fini(&ws->canvas);
	free_description(ws);
	free_buttons(ws);
	font_fini(ws);
	surface_fini(ws);
	compositor_fini(ws);
}

void read_stdin(struct waybutt_state *ws) {
	char buf[BUFSIZ], *p, *str_p;
	struct desc_line *line, **lines_end;
	uint32_t state = UTF8_ACCEPT, codepoint;
	uint16_t advance;
	size_t i, j, len;
	struct button *butt, *prev_butt, **butts_end;

	fgets(buf, sizeof buf, stdin);
	if((p = strchr(buf, '\n'))) *p = '\0';

	/* read buttons */
	butts_end = &ws->buttons;

	for (prev_butt = NULL, p = buf, i = 0;; i++) {
		if (buf[i] == ';') {
			buf[i] = '\0';

			butt = malloc(sizeof *butt);
			//if (!butt) return;

			butt->prev = prev_butt;
			butt->next = NULL;
			butt->text = strdup(strip_spaces_lr(p, buf + i - 1, &butt->highlighted));
			p = buf + (++i);

			// Make text run
			len = strlen(butt->text);
			uint32_t buf32[len - 1];

			for (len = 0, str_p = butt->text; *str_p; str_p++) {
				if (utf8_decode(&state, &codepoint, *str_p)) continue;

				buf32[len] = codepoint;
				len++;
			}

			butt->run = fcft_rasterize_text_run_utf32(ws->font,
				len, buf32, FCFT_SUBPIXEL_NONE);

			for (j = 0, advance = 0; j < len; j++) {
				advance += butt->run->glyphs[j]->advance.x;
			}
			butt->text_advance = advance;

			*butts_end = butt; butts_end = &butt->next;
			prev_butt = butt; butt = butt->next;
		}
		else if (!buf[i]) {
			butt = malloc(sizeof *butt);
			//if (!butt) return;

			butt->prev = prev_butt;
			butt->next = NULL;
			butt->text = strdup(strip_spaces_lr(p, buf + i - 1, &butt->highlighted));

			// Make text run
			len = strlen(butt->text);
			uint32_t buf32[len - 1];

			for (len = 0, str_p = butt->text; *str_p; str_p++) {
				if (utf8_decode(&state, &codepoint, *str_p)) continue;

				buf32[len] = codepoint;
				len++;
			}

			butt->run = fcft_rasterize_text_run_utf32(ws->font,
				len, buf32, FCFT_SUBPIXEL_NONE);

			for (j = 0, advance = 0; j < len; j++) {
				advance += butt->run->glyphs[j]->advance.x;
			}
			butt->text_advance = advance;

			*butts_end = butt; butts_end = &butt->next;
			ws->last = butt; p = buf + i;
			break;
		}
	}

	/* read description */
	for (lines_end = &ws->description; fgets(buf, sizeof buf, stdin);) {
		if((p = strchr(buf, '\n'))) *p = '\0';

		len = strlen(buf);
		if (!len) continue;

		uint32_t buf32[len - 1];

		line = malloc(sizeof *line);
		if (!line) return;

		for (len = 0, str_p = buf; *str_p; str_p++) {
			if (utf8_decode(&state, &codepoint, *str_p)) continue;

			buf32[len] = codepoint;
			len++;
		}

		line->run = fcft_rasterize_text_run_utf32(ws->font,
			len, buf32, FCFT_SUBPIXEL_NONE);

		for (i = 0, advance = 0; i < len; i++) {
			advance += line->run->glyphs[i]->advance.x;
		}

		if (ws->desc_width < advance) ws->desc_width = advance;
		*lines_end = line; lines_end = &line->next;
		ws->desc_lines++; line->next = NULL;
	}
}

void alarm_handler(int sig) {
	fprintf(stderr, "timed out!\n");
	exit(ExitTimedOut);
}

/*
 * MAIN
 */

int main(int argc, char **argv) {
	struct waybutt_state ws = {
		.fn_names = FnNames,

		.hex = {
			.bg   = ColBg,   .desc   = ColDesc,   .notice = ColNotice,
			.help = ColHelp, .border = ColBorder,

			.butt   = {ColButtonBg,       ColButtonFg,       ColButtonUl},
			.sbutt  = {ColSelButtonBg,    ColSelButtonFg,    ColSelButtonUl},
			.hbutt  = {ColHilButtonBg,    ColHilButtonFg,    ColHilButtonUl},
			.shbutt = {ColSelHilButtonBg, ColSelHilButtonFg, ColSelHilButtonUl},

			.image_layers  = {ColImageFg, ColImageShading, ColImageBorder},
		},
		.dim = {
			.line_height  = DimLineHeight,
			.text_level   = DimTextLevel,
			.border_width = BorderWidth,
			.button_ul_width = ButtonUnderlineWidth,
		},

		.help_str = HelpText,
		.notice_str = NULL,

		.image_width = KeyImageWidth,
		.image_height = KeyImageHeight,
		.image_type = ImageKey,

		.bottom = false,
		.desc_width = 0,
		.initial_sel_idx = 1,

		.run = RunRunning,
	};

	const char *usage =
		"Usage: waypass [-v] [-b] [-ik,il,ip] [-fn font]\n"
		"       [-o output] [-n notice] [-h height[:text_level]]\n"
		"       [-bw width] [-uw width] [-s index] [-t timeout]\n"
		"       [-bg,bc,dc,nc,hc color] [-Bc {colors}] [-Bs {colors}]\n"
		"       [-Hc {colors}] [-Hs {colors}] [-Ic {colors}]\n";

	for (size_t i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "-v")) { puts(VERSION); return 0; }
		if (!strcmp(argv[i], "-b")) ws.bottom = true;
		else if (!strncmp(argv[i], "-i", 2)) {

			switch (argv[i][2]) {
			case 'k':
				ws.image_type = ImageKey;
				ws.image_width = KeyImageWidth;
				ws.image_height = KeyImageHeight;
				break;
			case 'l':
				ws.image_type = ImagePadlock;
				ws.image_width = PadlockImageWidth;
				ws.image_height = PadlockImageHeight;
				break;
			case 'p':
				ws.image_type = ImagePower;
				ws.image_width = PowerImageWidth;
				ws.image_height = PowerImageHeight;
				break;
			default: goto print_usage;
			}
		}
		else if (i == argc - 1) goto print_usage;
		else if (!strcmp(argv[i], "-bw")) ws.dim.border_width = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-uw")) ws.dim.button_ul_width = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-h")) set_line_height(&ws.dim, argv[++i]);
		else if (!strcmp(argv[i], "-n")) ws.notice_str = argv[++i];
		else if (!strcmp(argv[i], "-o")) ws.output_name = argv[++i];
		else if (!strcmp(argv[i], "-s")) ws.initial_sel_idx = atoi(argv[++i]);
		else if (!strcmp(argv[i], "-t")) {
			signal(SIGALRM, alarm_handler);
			alarm(atoi(argv[++i]));
		}
		else if (!strcmp(argv[i], "-fn")) *(ws.fn_names)[0] = argv[++i];
		else if (!strcmp(argv[i], "-bg")) parse_hex_color(argv[++i], &ws.hex.bg);
		else if (!strcmp(argv[i], "-bc")) parse_hex_color(argv[++i], &ws.hex.border);
		else if (!strcmp(argv[i], "-dc")) parse_hex_color(argv[++i], &ws.hex.desc);
		else if (!strcmp(argv[i], "-nc")) parse_hex_color(argv[++i], &ws.hex.notice);
		else if (!strcmp(argv[i], "-hc")) parse_hex_color(argv[++i], &ws.hex.help);
		else if (!strcmp(argv[i], "-Bc")) parse_color_triad(argv[++i], ws.hex.butt);
		else if (!strcmp(argv[i], "-Bs")) parse_color_triad(argv[++i], ws.hex.sbutt);
		else if (!strcmp(argv[i], "-Hc")) parse_color_triad(argv[++i], ws.hex.hbutt);
		else if (!strcmp(argv[i], "-Hs")) parse_color_triad(argv[++i], ws.hex.shbutt);
		else if (!strcmp(argv[i], "-Ic")) parse_color_triad(argv[++i], ws.hex.image_layers);
		else goto print_usage;
	}

	font_init(&ws);
	read_stdin(&ws);
	waybutt_init(&ws, &ws.dim);
	waybutt_create_surface(&ws);

	draw_first_frame(&ws);

	struct pollfd fds[] = {
		{ wl_display_get_fd(ws.display), POLLIN },
		{ ws.repeat_timer, POLLIN },
	};

	const int nfds = sizeof(fds) / sizeof(*fds);

	while (ws.run == RunRunning) {
		errno = 0;

		do {
			if (wl_display_flush(ws.display) == -1 && errno != EAGAIN) {
				waybutt_fini(&ws);
				eprintf("wl_display_flush: %s\n", strerror(errno));
			}
		} while (errno == EAGAIN);

		if (poll(fds, nfds, -1) < 0) {
			waybutt_fini(&ws);
			eprintf("poll: %s\n", strerror(errno));
		}

		if (fds[0].revents & POLLIN) {
			if (wl_display_dispatch(ws.display) < 0) ws.run = false;
		}

		if (fds[1].revents & POLLIN) keyboard_repeat(&ws);
	}

	waybutt_fini(&ws);

	return ws.run == RunSuccess ? ExitSuccess : ExitFailure;

print_usage:
	fprintf(stderr, "%s", usage);
	return 1;
}
