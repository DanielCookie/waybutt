# waybutt

Wayland utility that generates a button selection from stdin

Read manpage for keybindings and usage flags.

## Usage

Stdin looks like this
```
button1; !button2; button3
description text
will be shown exactly as given,
with newlines and all
```

The first line has the buttons' content.
An exclamation mark before the button text makes the button
The description starts at the second line.

Also, you can insert a line of highlighted standout text with the
`-n` (notice) flag. This line appears right below the description
and above the buttons:
```
waybutt -n 'important info' < file
```

To see the examples, run:

```
waybutt < ./examples/himitsu.txt
waybutt < ./examples/gnupg.txt
waybutt < ./examples/colors.txt
```

## Building

Run `samu` or `ninja` in the repo directory.
Configure built-in settings in config.h

## Future plans

Integrate this program's functionality into the uprompt binary.
