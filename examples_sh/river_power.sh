#!/bin/sh

action=$(\
	echo 'REBOOT;!HALT;TTY\nChange Power State:' |
	waybutt -b -bw 0 -ip \
	  -bg 040404ed \
	  -Bc 040404ed,8787A6,161616ed \
	  -Bs 040404ed,A39CD9,282A58ed \
	  -Hc 040404ed,EBCB8B,161616ed \
	  -Hs 040404ed,EB8BB2,EB8BB2ed \
) || exit 1

$XDG_CONFIG_HOME/river/cleanup

case $action in
	REBOOT) setsid doas /usr/bin/shutdown -r now ;;
	HALT)   setsid doas /usr/bin/shutdown -h now ;;
	TTY)    riverctl exit ;;
esac
